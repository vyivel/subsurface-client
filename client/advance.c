#include "base.h"

static struct subsurface white_subsurface;
static struct subsurface red_subsurface;
static struct subsurface green_subsurface;

static int stage = 0;

static void advance() {
    static const char *stage_names[] = {
            [1] = "adding white subsurface to window surface",
            [2] = "committing window surface",
            [3] = "adding red and green subsurfaces to white subsurface",
            [4] = "committing white subsurface",
            [5] = "committing window surface",
    };
    static const char *expected[] = {
            [1] = "black",
            [2] = "black, white",
            [3] = "black, white",
            [4] = "black, white",
            [5] = "black, white, red, green",
    };
    if (stage == sizeof(stage_names) / sizeof(*stage_names) - 1) {
        return;
    }
    ++stage;
    printf("Stage %d: %s, expected state: %s\n", stage, stage_names[stage],
            expected[stage]);
    switch (stage) {
    case 1:
        subsurface_init(&white_subsurface, &window.surface, 20, 20,
                (float[]){1.0f, 1.0f, 1.0f});
        break;
    case 2:
        wl_surface_commit(window.surface.wl_surface);
        break;
    case 3:
        subsurface_init(&red_subsurface, &white_subsurface.surface, 20, 20,
                (float[]){1.0f, 0.0f, 0.0f});
        subsurface_init(&green_subsurface, &white_subsurface.surface, 40, 40,
                (float[]){0.0f, 1.0f, 0.0f});
        break;
    case 4:
        wl_surface_commit(white_subsurface.surface.wl_surface);
        break;
    case 5:
        wl_surface_commit(window.surface.wl_surface);
        break;
    };
}

static void handle_key(uint32_t key, uint32_t state) {
    if (state == WL_KEYBOARD_KEY_STATE_PRESSED && key == 28) {
        /* Pressed enter */
        advance();
    }
}

int main() {
    user_key_handler = handle_key;
    base_init();

    printf("Press Enter to advance through stages.\n");
    printf("Refrain from resizing the window after the first stage.\n");

    while (wl_display_dispatch(display) != -1) {
        /* Wait */
    }
    return 0;
}
