#include "base.h"

static struct subsurface child;

static int nbuffers = 0;

static void handle_key(uint32_t key, uint32_t state) {
    if (state == WL_KEYBOARD_KEY_STATE_PRESSED && key == 28) {
        /* Pressed enter */
        ++nbuffers;
        printf("Trying to commit %d buffer(s)...\n", nbuffers);
        surface_render(&child.surface);
    }
}

int main() {
    user_key_handler = handle_key;
    base_init();

    printf("Press Enter a few times to commit child subsurface buffers.\n");
    printf("You are not supposed to see \"failed to obtain buffer\" "
           "messages.\n");

    subsurface_init(
            &child, &window.surface, 20, 20, (float[]){1.0f, 0.0f, 1.0f, 1.0f});
    wl_surface_commit(window.surface.wl_surface);

    while (wl_display_dispatch(display) != -1) {
        /* Wait */
    }
    return 0;
}
