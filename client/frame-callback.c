#include "base.h"

static struct subsurface child;

static int stage = 0;

static void callback_done(
        void *data, struct wl_callback *wl_callback, uint32_t callback_data) {
    printf("Frame callback\n");
}

static struct wl_callback_listener callback_listener = {
        .done = callback_done,
};

static void advance() {
    ++stage;
    if (stage == 1) {
        struct wl_callback *callback
                = wl_surface_frame(child.surface.wl_surface);
        wl_callback_add_listener(callback, &callback_listener, NULL);
        child.surface.color[0] = 1.0f;
        child.surface.color[1] = 1.0f;
        child.surface.color[2] = 0.0f;
        surface_render(&child.surface);
        printf("Scheduled a frame callback on child and committed it\n");
    } else if (stage == 2) {
        wl_surface_commit(window.surface.wl_surface);
        printf("Committed window surface\n");
    }
}

static void handle_key(uint32_t key, uint32_t state) {
    if (state == WL_KEYBOARD_KEY_STATE_PRESSED && key == 28) {
        /* Pressed enter */
        advance();
    }
}

int main() {
    user_key_handler = handle_key;
    base_init();

    printf("Press Enter to advance through stages.\n");
    printf("Refrain from resizing the window after the first stage.\n");

    subsurface_init(
            &child, &window.surface, 20, 20, (float[]){1.0f, 1.0f, 1.0f});
    wl_surface_commit(window.surface.wl_surface);

    while (wl_display_dispatch(display) != -1) {
        /* Wait */
    }
    return 0;
}
