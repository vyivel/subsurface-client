#include "base.h"

static struct subsurface child;
static struct subsurface grandchild;

static struct wl_callback_listener callback_listener;

static int nframes = 0;

static void callback_done(
        void *data, struct wl_callback *wl_callback, uint32_t callback_data) {
    float red = (sin(nframes / 100.f) + 1) / 2;
    grandchild.surface.color[0] = red;
    ++nframes;

    struct wl_callback *callback
            = wl_surface_frame(grandchild.surface.wl_surface);
    wl_callback_add_listener(callback, &callback_listener, NULL);

    surface_render(&grandchild.surface);
}

static struct wl_callback_listener callback_listener = {
        .done = callback_done,
};

int main() {
    base_init();

    subsurface_init(
            &child, &window.surface, 200, 200, (float[]){1.0f, 1.0f, 1.0f});
    subsurface_init(
            &grandchild, &child.surface, 200, 200, (float[]){0.0f, 0.0f, 1.0f});
    wl_surface_commit(child.surface.wl_surface);
    wl_surface_commit(window.surface.wl_surface);

    wl_subsurface_set_desync(child.wl_subsurface);
    wl_subsurface_set_desync(grandchild.wl_subsurface);

    struct wl_callback *callback
            = wl_surface_frame(grandchild.surface.wl_surface);
    wl_callback_add_listener(callback, &callback_listener, NULL);
    wl_surface_commit(grandchild.surface.wl_surface);

    while (wl_display_dispatch(display) != -1) {
        /* Wait */
    }
    return 0;
}
