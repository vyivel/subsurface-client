#include "base.h"

static struct subsurface child;
static struct subsurface grandchild;

static void handle_key(uint32_t key, uint32_t state) {
    if (state == WL_KEYBOARD_KEY_STATE_PRESSED && key == 28) {
        /* Pressed enter */

        /* >Buffer [red] is attached and committed on surface [grandchild] */
        grandchild.surface.color[0] = 1.0f;
        grandchild.surface.color[1] = 0.0f;
        grandchild.surface.color[2] = 0.0f;
        surface_render(&grandchild.surface);

        /* >Surface [child] is committed */
        wl_surface_commit(child.surface.wl_surface);

        /* >Buffer [blue] is attached and committed on surface [grandchild] */
        grandchild.surface.color[0] = 0.0f;
        grandchild.surface.color[1] = 0.0f;
        grandchild.surface.color[2] = 1.0f;
        surface_render(&grandchild.surface);

        /* >Surface [window] is committed */
        wl_surface_commit(window.surface.wl_surface);
    }
}

int main() {
    user_key_handler = handle_key;
    base_init();

    /* [window] -> [child] -> [grandchild] */
    subsurface_init(
            &child, &window.surface, 20, 20, (float[]){1.0f, 1.0f, 1.0f});
    subsurface_init(
            &grandchild, &child.surface, 20, 20, (float[]){0.0f, 0.0f, 0.0f});
    wl_surface_commit(child.surface.wl_surface);
    wl_surface_commit(window.surface.wl_surface);

    while (wl_display_dispatch(display) != -1) {
        /* Wait */
    }
    return 0;
}
