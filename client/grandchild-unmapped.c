#include "base.h"

static struct subsurface child;
static struct subsurface grandchild;

int main() {
    base_init();

    surface_init(&child.surface, 400, 400, (float[]){1.0f, 1.0f, 1.0f});
    child.wl_subsurface = wl_subcompositor_get_subsurface(
            subcompositor, child.surface.wl_surface, window.surface.wl_surface);
    wl_subsurface_set_position(child.wl_subsurface, 20, 20);

    subsurface_init(
            &grandchild, &child.surface, 20, 20, (float[]){0.0f, 1.0f, 0.0f});

    wl_surface_commit(child.surface.wl_surface);
    wl_surface_commit(window.surface.wl_surface);

    while (wl_display_dispatch(display) != -1) {
        /* Wait */
    }
    return 0;
}
