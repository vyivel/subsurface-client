#include "base.h"

static struct subsurface red_subsurface;
static struct subsurface green_subsurface;

int main() {
    base_init();

    subsurface_init(&red_subsurface, &window.surface, 20, 20,
            (float[]){1.0f, 0.0f, 0.0f});
    subsurface_init(&green_subsurface, &window.surface, 40, 40,
            (float[]){0.0f, 1.0f, 0.0f});
    wl_subsurface_place_above(
            green_subsurface.wl_subsurface, red_subsurface.surface.wl_surface);
    wl_surface_commit(window.surface.wl_surface);

    printf("The expected order is: black, red, green.\n");

    while (wl_display_dispatch(display) != -1) {
        /* Wait */
    }
    return 0;
}
