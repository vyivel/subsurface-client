#include "base.h"

static struct subsurface child;
static struct subsurface grandchild;

static struct xdg_popup *popup;
static struct xdg_surface *popup_xdg_surface;
static struct surface popup_surface;

static struct xdg_positioner *positioner;

static void reset_popup() {
    xdg_popup_destroy(popup);
    popup = NULL;
    xdg_surface_destroy(popup_xdg_surface);
    popup_xdg_surface = NULL;
    wl_surface_destroy(popup_surface.wl_surface);
    popup_surface.wl_surface = NULL;
}

static void xdg_surface_handle_configure(
        void *data, struct xdg_surface *xdg_surface, uint32_t serial) {
    xdg_surface_ack_configure(xdg_surface, serial);
    surface_render(&popup_surface);
}

static struct xdg_surface_listener xdg_surface_listener = {
        .configure = xdg_surface_handle_configure,
};

static void xdg_popup_handle_popup_done(void *data,
        struct xdg_popup *xdg_popup) {
    reset_popup();
}

static void xdg_popup_handle_configure(void *data,
        struct xdg_popup *xdg_popup, int32_t x, int32_t y,
        int32_t width, int32_t height) {
    if (width == 0 || height == 0) {
        return;
    }
    popup_surface.width = width;
    popup_surface.height = height;
}

static void xdg_popup_handle_repositioned(void *data,
        struct xdg_popup *xdg_popup, uint32_t token) {
    /* nop */
}

static const struct xdg_popup_listener xdg_popup_listener = {
    .popup_done = xdg_popup_handle_popup_done,
    .configure = xdg_popup_handle_configure,
    .repositioned = xdg_popup_handle_repositioned,
};

static void create_popup() {
    if (popup) {
        reset_popup();
    }

    surface_init(&popup_surface, 300, 300, (float[]){1.0f, 0.0f, 0.5f});

    subsurface_init(
            &child, &popup_surface, 200, 200, (float[]){1.0f, 1.0f, 1.0f});
    subsurface_init(
            &grandchild, &child.surface, 200, 200, (float[]){0.0f, 0.0f, 1.0f});

    surface_render(&grandchild.surface);
    surface_render(&child.surface);

    popup_xdg_surface = xdg_wm_base_get_xdg_surface(wm_base,
            popup_surface.wl_surface);
    xdg_surface_add_listener(popup_xdg_surface, &xdg_surface_listener, NULL);
    popup = xdg_surface_get_popup(popup_xdg_surface,
            window.xdg_surface, positioner);
    xdg_popup_add_listener(popup, &xdg_popup_listener, NULL);
    wl_surface_commit(popup_surface.wl_surface);
}

static void handle_key(uint32_t key, uint32_t state) {
    if (state == WL_KEYBOARD_KEY_STATE_PRESSED && key == 28) {
        /* Pressed enter */
        create_popup();
    }
}

int main() {
    user_key_handler = handle_key;
    base_init();

    positioner = xdg_wm_base_create_positioner(wm_base);
    xdg_positioner_set_size(positioner, 400, 200);
    xdg_positioner_set_anchor_rect(positioner, 0, 0, 400, 200);

    printf("Press Enter to open a popup.\n");

    while (wl_display_dispatch(display) != -1) {
        /* Wait */
    }
    return 0;
}
