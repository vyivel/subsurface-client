#include "base.h"

static struct subsurface child;

static int stage = 0;

static void advance() {
    ++stage;
    if (stage == 1) {
        wl_subsurface_set_position(child.wl_subsurface, -20, -20);
        wl_surface_commit(child.surface.wl_surface);
        printf("Moved child and committed it\n");
    } else if (stage == 2) {
        wl_surface_commit(window.surface.wl_surface);
        printf("Committed window surface\n");
    }
}

static void handle_key(uint32_t key, uint32_t state) {
    if (state == WL_KEYBOARD_KEY_STATE_PRESSED && key == 28) {
        /* Pressed enter */
        advance();
    }
}

int main() {
    user_key_handler = handle_key;
    base_init();

    printf("Press Enter to advance through stages.\n");
    printf("Refrain from resizing the window after the first stage.\n");

    subsurface_init(
            &child, &window.surface, 20, 20, (float[]){1.0f, 1.0f, 1.0f});
    wl_subsurface_set_desync(child.wl_subsurface);
    wl_surface_commit(window.surface.wl_surface);

    while (wl_display_dispatch(display) != -1) {
        /* Wait */
    }
    return 0;
}
