#include <assert.h>

#include "base.h"

static struct subsurface child;

static int stage = 0;

static void feedback_sync_output(void *data,
        struct wp_presentation_feedback *feedback, struct wl_output *output) {
    /* nop */
}

static void feedback_presented(void *data,
        struct wp_presentation_feedback *feedback, uint32_t tv_sec_hi,
        uint32_t tv_sec_lo, uint32_t tv_nsec, uint32_t refresh, uint32_t seq_hi,
        uint32_t seq_lo, uint32_t flags) {
    uint64_t fb_id = (uint64_t)data;
    printf("Presented #%lu\n", fb_id);
}

static void feedback_discarded(
        void *data, struct wp_presentation_feedback *feedback) {
    uint64_t fb_id = (uint64_t)data;
    printf("Discarded #%lu\n", fb_id);
}

static const struct wp_presentation_feedback_listener feedback_listener = {
        .sync_output = feedback_sync_output,
        .presented = feedback_presented,
        .discarded = feedback_discarded,
};

static void advance() {
    ++stage;
    if (stage == 1) {
        printf("Committing the child subsurface with feedback #1\n");
        struct wp_presentation_feedback *feedback = wp_presentation_feedback(
                presentation, child.surface.wl_surface);
        wp_presentation_feedback_add_listener(
                feedback, &feedback_listener, (void *)1);
        surface_render(&child.surface);
    } else if (stage == 2) {
        printf("Committing the child subsurface with feedback #2\n");
        struct wp_presentation_feedback *feedback = wp_presentation_feedback(
                presentation, child.surface.wl_surface);
        wp_presentation_feedback_add_listener(
                feedback, &feedback_listener, (void *)2);
        surface_render(&child.surface);
    } else if (stage == 3) {
        printf("Committing the window surface\n");
        wl_surface_commit(window.surface.wl_surface);
    }
}

static void handle_key(uint32_t key, uint32_t state) {
    if (state == WL_KEYBOARD_KEY_STATE_PRESSED && key == 28) {
        /* Pressed enter */
        advance();
    }
}

int main() {
    user_key_handler = handle_key;
    base_init();
    assert(presentation);

    subsurface_init(
            &child, &window.surface, 20, 20, (float[]){1.0f, 1.0f, 0.0f});
    wl_surface_commit(window.surface.wl_surface);

    while (wl_display_dispatch(display) != -1) {
        /* Wait */
    }
    return 0;
}
