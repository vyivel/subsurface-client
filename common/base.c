#include "base.h"

struct wl_display *display = NULL;
struct wl_shm *shm = NULL;
struct wl_compositor *compositor = NULL;
struct wl_subcompositor *subcompositor = NULL;
struct wp_viewporter *viewporter = NULL;
struct xdg_wm_base *wm_base = NULL;
struct wp_presentation *presentation = NULL;
struct wl_data_device_manager *data_device_manager = NULL;
struct wl_seat *seat = NULL;
struct wl_keyboard *keyboard = NULL;

struct window window;

void (*user_key_handler)(uint32_t key, uint32_t state) = NULL;
void (*user_configure_handler)() = NULL;
void (*user_renderer)(
        struct surface *surface, struct pool_buffer *buffer, cairo_t *cairo)
        = NULL;

void surface_init(
        struct surface *surface, int width, int height, float *color) {
    surface->wl_surface = wl_compositor_create_surface(compositor);
    surface->width = width;
    surface->height = height;
    memcpy(&surface->color, color, sizeof(surface->color));
};

void surface_render(struct surface *surface) {
    struct pool_buffer *buffer = get_next_buffer(
            shm, surface->buffers, surface->width, surface->height);
    if (buffer == NULL) {
        fprintf(stderr, "failed to obtain buffer\n");
        return;
    }

    cairo_t *cairo = buffer->cairo;

    cairo_save(cairo);

    if (user_renderer) {
        user_renderer(surface, buffer, cairo);
    } else {
        float *color = surface->color;
        cairo_set_operator(cairo, CAIRO_OPERATOR_SOURCE);
        cairo_set_source_rgba(cairo, color[0], color[1], color[2], 1.0f);
        cairo_paint(cairo);
        wl_surface_attach(surface->wl_surface, buffer->buffer, 0, 0);
        buffer->busy = true;
    }

    cairo_restore(cairo);

    wl_surface_damage_buffer(
            surface->wl_surface, 0, 0, surface->width, surface->height);
    wl_surface_commit(surface->wl_surface);
}

void subsurface_init(struct subsurface *subsurface, struct surface *parent,
        int x, int y, float *color) {
    surface_init(&subsurface->surface, 400, 400, color);
    subsurface->wl_subsurface = wl_subcompositor_get_subsurface(
            subcompositor, subsurface->surface.wl_surface, parent->wl_surface);
    wl_subsurface_set_position(subsurface->wl_subsurface, x, y);
    surface_render(&subsurface->surface);
}

static void keyboard_handle_enter(void *data, struct wl_keyboard *wl_keyboard,
        uint32_t serial, struct wl_surface *surface, struct wl_array *keys) {
    /* nop */
}

static void keyboard_handle_leave(void *data, struct wl_keyboard *wl_keyboard,
        uint32_t serial, struct wl_surface *surface) {
    /* nop */
}

static void keyboard_handle_key(void *data, struct wl_keyboard *wl_keyboard,
        uint32_t serial, uint32_t time, uint32_t key, uint32_t state) {
    if (user_key_handler) {
        user_key_handler(key, state);
    }
}

static void keyboard_handle_modifiers(void *data,
        struct wl_keyboard *wl_keyboard, uint32_t serial,
        uint32_t mods_depressed, uint32_t mods_latched, uint32_t mods_locked,
        uint32_t group) {
    /* nop */
}

static void keyboard_handle_keymap(void *data, struct wl_keyboard *wl_keyboard,
        uint32_t format, int32_t fd, uint32_t size) {
    /* nop */
}

static void keyboard_handle_repeat_info(void *data,
        struct wl_keyboard *wl_keyboard, int32_t rate, int32_t delay) {
    /* nop */
}

static const struct wl_keyboard_listener keyboard_listener = {
        .enter = keyboard_handle_enter,
        .leave = keyboard_handle_leave,
        .key = keyboard_handle_key,
        .modifiers = keyboard_handle_modifiers,
        .keymap = keyboard_handle_keymap,
        .repeat_info = keyboard_handle_repeat_info,
};

static void seat_handle_capabilities(
        void *data, struct wl_seat *seat, uint32_t capabilities) {
    if (capabilities & WL_SEAT_CAPABILITY_KEYBOARD) {
        if (keyboard != NULL) {
            return;
        }
        keyboard = wl_seat_get_keyboard(seat);
        wl_keyboard_add_listener(keyboard, &keyboard_listener, NULL);
    }
}

static const struct wl_seat_listener seat_listener = {
        .capabilities = seat_handle_capabilities,
};

static void wm_base_handle_ping(
        void *data, struct xdg_wm_base *xdg_wm_base, uint32_t serial) {
    xdg_wm_base_pong(xdg_wm_base, serial);
}

static const struct xdg_wm_base_listener wm_base_listener = {
        .ping = wm_base_handle_ping,
};

static void presentation_handle_clock_id(
        void *data, struct wp_presentation *wp_presentation, uint32_t clk_id) {
    /* nop */
}

static const struct wp_presentation_listener presentation_listener = {
        .clock_id = presentation_handle_clock_id,
};

static void handle_global(void *data, struct wl_registry *registry,
        uint32_t name, const char *interface, uint32_t version) {
    if (strcmp(interface, wl_shm_interface.name) == 0) {
        shm = wl_registry_bind(registry, name, &wl_shm_interface, 1);
    } else if (strcmp(interface, wl_compositor_interface.name) == 0) {
        compositor
                = wl_registry_bind(registry, name, &wl_compositor_interface, 4);
    } else if (strcmp(interface, wl_subcompositor_interface.name) == 0) {
        subcompositor = wl_registry_bind(
                registry, name, &wl_subcompositor_interface, 1);
    } else if (strcmp(interface, wp_viewporter_interface.name) == 0) {
        viewporter
                = wl_registry_bind(registry, name, &wp_viewporter_interface, 1);
    } else if (strcmp(interface, xdg_wm_base_interface.name) == 0) {
        wm_base = wl_registry_bind(registry, name, &xdg_wm_base_interface, 1);
        xdg_wm_base_add_listener(wm_base, &wm_base_listener, NULL);
    } else if (strcmp(interface, wp_presentation_interface.name) == 0) {
        presentation = wl_registry_bind(
                registry, name, &wp_presentation_interface, 1);
        wp_presentation_add_listener(
                presentation, &presentation_listener, NULL);
    } else if (strcmp(interface, wl_seat_interface.name) == 0) {
        seat = wl_registry_bind(registry, name, &wl_seat_interface, 1);
        wl_seat_add_listener(seat, &seat_listener, NULL);
    } else if (strcmp(interface, wl_data_device_manager_interface.name) == 0) {
        data_device_manager = wl_registry_bind(
                registry, name, &wl_data_device_manager_interface, 3);
    }
}

static void handle_global_remove(
        void *data, struct wl_registry *registry, uint32_t name) {
    /* nop */
}

static const struct wl_registry_listener registry_listener = {
        .global = handle_global,
        .global_remove = handle_global_remove,
};

static void xdg_surface_handle_configure(
        void *data, struct xdg_surface *xdg_surface, uint32_t serial) {
    xdg_surface_ack_configure(xdg_surface, serial);
    printf("!!! Committing due to rerender !!!\n");
    surface_render(&window.surface);
}

static const struct xdg_surface_listener xdg_surface_listener = {
        .configure = xdg_surface_handle_configure,
};

static void xdg_toplevel_handle_configure(void *data,
        struct xdg_toplevel *xdg_toplevel, int32_t w, int32_t h,
        struct wl_array *states) {
    if (w == 0 || h == 0) {
        return;
    }
    window.surface.width = w;
    window.surface.height = h;
    if (user_configure_handler) {
        user_configure_handler();
    }
}

static void xdg_toplevel_handle_close(
        void *data, struct xdg_toplevel *xdg_toplevel) {
    exit(0);
}

static const struct xdg_toplevel_listener xdg_toplevel_listener = {
        .configure = xdg_toplevel_handle_configure,
        .close = xdg_toplevel_handle_close,
};

void base_init() {
    display = wl_display_connect(NULL);
    if (!display) {
        fprintf(stderr, "Couldn't connect\n'");
        exit(1);
    }

    struct wl_registry *registry = wl_display_get_registry(display);
    wl_registry_add_listener(registry, &registry_listener, NULL);
    wl_display_dispatch(display);
    wl_display_roundtrip(display);

    if (shm == NULL || compositor == NULL || wm_base == NULL) {
        fprintf(stderr, "Couldn't get required globals\n");
        exit(1);
    }

    surface_init(&window.surface, 300, 300, (float[]){0.0f, 0.0f, 0.0f});
    window.xdg_surface
            = xdg_wm_base_get_xdg_surface(wm_base, window.surface.wl_surface);
    xdg_surface_add_listener(window.xdg_surface, &xdg_surface_listener, NULL);

    window.xdg_toplevel = xdg_surface_get_toplevel(window.xdg_surface);
    xdg_toplevel_add_listener(
            window.xdg_toplevel, &xdg_toplevel_listener, NULL);

    wl_surface_commit(window.surface.wl_surface);
};
